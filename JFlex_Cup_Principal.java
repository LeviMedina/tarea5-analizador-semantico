
package JFlex_Cup;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JFlex_Cup_Principal {

    
    public static void main(String[] args) throws Exception {
        Interfaz_JFlex_Cup.main(args);
       
    }
    
    public static void generarLexer() throws Exception{
        // Ruta del archivo donde se encuentra el archivo lex.flex
        String[] ruta = {"C:\\Users\\52981\\Documents\\NetBeansProjects\\src\\Tarea5_JFlex\\src\\JFlex_1\\lex.flex"};
        try {
            jflex.Main.generate(ruta);
        }catch(Exception e){
            throw new Exception("Error al crear Archivo Lexer.java ");
        }
    }


    public static void generar() throws IOException, Exception{
        String rutaProy = "\\Users\\52981\\Documents\\NetBeansProjects\\Tarea5_JFlex";
        String[] ruta1 = {rutaProy+"/src/Tarea5_JFlex/lex.flex"};
        String[] ruta2 = {rutaProy+"/src/Tarea5_JFlex/LexerCup.flex"};
        String[] rutaS = {"-parser", "Sintax", rutaProy+"/src/Tarea5_JFlex/Sintax.cup"};

        jflex.Main.generate(ruta1);       
        jflex.Main.generate(ruta2);
        java_cup.Main.main(rutaS);
        
        Path rutaSym = Paths.get(rutaProy+"/src/Tarea5_JFlex/sym.java");
        if (Files.exists(rutaSym)) {
            Files.delete(rutaSym);
        }
        Files.move(
                Paths.get(rutaProy+"/sym.java"), 
                Paths.get(rutaProy+"/src/Tarea5_JFlex/sym.java")
        );
        Path rutaSin = Paths.get(rutaProy+"/src/Tarea5_JFlex/Sintax.java");
        if (Files.exists(rutaSin)) {
            Files.delete(rutaSin);
        }
        Files.move(
                Paths.get(rutaProy+"/Sintax.java"), 
                Paths.get(rutaProy+"/src/Tarea5_JFlex/Sintax.java")
        );
    }

    
}
